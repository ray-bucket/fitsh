var assert;
var fitsh;
var fetch;

if (typeof window === 'undefined') {
  assert = require('chai').assert;
  fitsh = require('../src/index').fitsh;
  fetch = require('node-fetch');
} else {
  assert = chai.assert;
  fetch = window.fetch;
}

const requestJSON = fitsh(fetch, 'https://jsonplaceholder.typicode.com', {
  mode: 'cors',
  headers: { 'X-TEST': 'test' },
});

fitsh.fetch = fetch;
const requestText = fitsh(/*fetch, */'https://jsonplaceholder.typicode.com', {
  mode: 'cors',
  headers: { 'X-TEST': 'test' },
  responseAs: 'text',
});

describe('fitsh', () => {

  describe('requestText(posts) [text]', () => {
    const postsStr = requestText('posts');

    it('should #get()', (done) => {
      postsStr
        .get('text')
        .then((data) => {
          assert(data.substring(0, 1) === '[');
          done();
        })
        .catch(done);
    });
  });

  describe('requestJSON(posts) [json]', () => {
    const posts = requestJSON('posts');

    it('should #get()', (done) => {
      posts
        .get()
        .then((arr) => {
          assert(arr.length);
          done();
        })
        .catch(done);
    });

    it('should #get(1)', (done) => {
      posts(1)
        .get()
        .then((obj) => {
          assert.property(obj, 'title');
          done();
        })
        .catch(done);
    });

    it('should #get({ query: })', (done) => {
      posts
        .get({ userId: 1 })
        .then((arr) => {
          assert.lengthOf(arr, 10);
          done();
        })
        .catch(done);
    });

    it('should #post({ data: }', (done) => {
      posts
        .post({ title: 'foo' })
        .then((obj) => {
          assert.property(obj, 'id');
          done();
        })
        .catch(done);
    });

    it('should #put({ data: })', (done) => {
      posts(1)
        .put({ title: 'foo' })
        .then((obj) => {
          assert.propertyVal(obj, 'title', 'foo');
          done();
        })
        .catch(done);
    });

    it('should #patch({ data: })', (done) => {
      posts(1)
        .patch({ title: 'foo' })
        .then((obj) => {
          assert.propertyVal(obj, 'title', 'foo');
          done();
        })
        .catch(done);
    });

    it('should #delete()', (done) => {
      posts(1)
        .delete()
        .then((obj) => {
          assert.deepEqual(obj, {});
          done();
        })
        .catch(done);
    });
  });

  describe('requestJSON(posts/1/comments)', () => {
    const posts = requestJSON('posts');
    const comments = posts(`${1}/comments`);

    it('should #get()', (done) => {
      comments
        .get()
        .then((arr) => {
          assert(arr.length);
          done();
        });
    });
  });

  describe('requestJSON(not/found)', () => {
    const notFound = requestJSON('not/found');

    it('should fail with 404', (done) => {
      notFound
        .get()
        .catch((err) => {
          assert.equal(err.response.status, 404);
          assert.equal(err.request.method, 'GET');
          done();
        });
    });
  });
});
