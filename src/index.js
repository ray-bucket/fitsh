const to = async (promise) => {
  try {
    return await promise;
  } catch {
    return null;
  }
};

const makeParam = (key, value) => `${key}=${encodeURIComponent(value)}`;

const makeQuery = (params) => Object
  .entries(params)
  .map(([key, value]) => (value instanceof Array
    ? value.map((item) => makeParam(key, item))
    : makeParam(key, value)))
  .flat()
  .join('&');

const mergeOptions = (base, override) => ({
  ...base,
  ...override,
  headers: {
    ...(base ? base.headers || {} : {}),
    ...(override ? override.headers || {} : {}),
  },
});

const req = (fetch, method, url, options, data, query, as) => {
  const mergedOptions = mergeOptions({
    body: data ? JSON.stringify(data) : undefined,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    method: method.toUpperCase(),
  }, options);

  const allUrl = query ? `${url}?${makeQuery(query)}` : url;

  return fetch(allUrl, mergedOptions).then(async (response) => {
    const parseResponse = () => {
      if (as === 'raw') { return response; }
      if (response.status === 204) { return null; }
      return response[as]();
    };
    if (response.status >= 200 && response.status <= 300) {
      return parseResponse();
    }

    const error = Object.assign(new Error(response.statusText), {
      response,
      content: await to(parseResponse()),
      request: {
        ...mergedOptions,
        url: allUrl,
      },
    });

    throw error;
  });
};

const getMethod = (a) => (typeof a === 'string' ? a : 'json');

function fitsh(p1, p2, p3) {
  if (!fitsh.fetch && typeof p1 !== 'function' && typeof window === 'undefined') {
    throw new Error('param 1 must be a fetch on node');
  }

  const fetch = typeof p1 === 'function' ? p1 : (fitsh.fetch || window.fetch);
  const url = typeof p1 === 'string' ? p1 : p2;
  const opts = p3 || p2;
  const f = (segment, overrideOptions) => fitsh(fetch, `${url}/${segment}`, mergeOptions(opts, overrideOptions));

  f.get = (a, b) => req(fetch, 'get', url, opts, null, b || a, getMethod(a));
  f.post = (a, b) => req(fetch, 'post', url, opts, b || a, null, getMethod(a));
  f.put = (a, b) => req(fetch, 'put', url, opts, b || a, null, getMethod(a));
  f.patch = (a, b) => req(fetch, 'patch', url, opts, b || a, null, getMethod(a));
  f.delete = (a, b) => req(fetch, 'delete', url, opts, b || a, null, getMethod(a));

  return f;
}

if (typeof exports === 'object') {
  module.exports = { fitsh };
} else {
  window.fitsh = fitsh;
}
