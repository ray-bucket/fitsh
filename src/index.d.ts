export type FitshMethod = 'get' | 'post' | 'put' | 'patch' | 'delete';

export type FitshResponseType = 'arrayBuffer' | 'blob' | 'formData' | 'json' | 'text' | 'raw';

export type ResponseType<Type extends FitshResponseType> = {
  arrayBuffer: ArrayBuffer;
  blob: Blob;
  formData: FormData;
  json: any;
  raw: Response,
  text: string;
}[Type];

export type FitshResponse<Type extends FitshResponseType> = Promise<ResponseType<Type>>;

export type FitshData = Record<string, any>;

export interface Fitsh {
  (url: string, options?: RequestInit): Fitsh;
  get(data?: Record<string, any>): Promise<ResponseType<'json'>>;
  get<T extends FitshResponseType>(as: T, data?: FitshData): FitshResponse<T>;

  post(data?: Record<string, any>): Promise<ResponseType<'json'>>;
  post<T extends FitshResponseType>(as: T, data?: FitshData): FitshResponse<T>;

  put(data?: Record<string, any>): Promise<ResponseType<'json'>>;
  put<T extends FitshResponseType>(as: T, data?: FitshData): FitshResponse<T>;

  patch(data?: Record<string, any>): Promise<ResponseType<'json'>>;
  patch<T extends FitshResponseType>(as: T, data?: FitshData): FitshResponse<T>;

  delete(data?: Record<string, any>): Promise<ResponseType<'json'>>;
  delete<T extends FitshResponseType>(as: T, data?: FitshData): FitshResponse<T>;
}

export interface FitshFunction {
  (url: string, options?: RequestInit): Fitsh;
  (fetch: Function, url: string, options?: RequestInit): Fitsh;
  /**
   * fetch function for internal use
   */
  fetch?: Function;
}

declare global {
  interface Window {
    fitsh: FitshFunction;
  }
}

export interface FitshError extends Error {
  response: Response;
}

declare const fitsh: FitshFunction;
